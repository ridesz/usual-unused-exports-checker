#!/usr/bin/env node

import * as shellJs from "shelljs";
import chalk from "chalk";

import analyzeTsConfig from "ts-unused-exports";

function getProjectFolder(): string {
    const result = shellJs.pwd();
    if (result.code !== 0) {
        throw new Error("Can't get the project folder path");
    }
    return result.stdout;
}

const projectFolder = getProjectFolder();

function checkUnusedExports(): void {
    const result = analyzeTsConfig(`${projectFolder}/tsconfig.json`);
    const setOfAffectedFiles = new Set<string>();
    const unusedExports: Array<[string, string]> = [];
    for (const key in result) {
        const value = result[key];
        if (value !== undefined) {
            if (key.endsWith("/src/index.ts")) {
                console.log("Unused exports in the index.ts file of the root src folder is OK.");
            } else {
                setOfAffectedFiles.add(key);
                value.forEach((v) => {
                    unusedExports.push([key, v.exportName]);
                });
            }
        }
    }
    if (setOfAffectedFiles.size > 0) {
        console.log();
        console.log(chalk.red(`Unused exports in the following cases:`));
        console.log();
        unusedExports.forEach((u) => {
            console.log(chalk.yellow(`${u[0]} -> ${u[1]}`));
        });
        console.log();
        console.log(chalk.red(`${setOfAffectedFiles.size} file affected by unused exports.`));
        console.log();
        throw new Error("Unused exports detected.");
    } else {
        console.log();
        console.log(chalk.green("No unused exports are detected!"));
        console.log();
    }
}

async function main(): Promise<void> {
    try {
        console.log("Checking unused exports");

        checkUnusedExports();

        console.log();
        console.log("Unused exports check finished.");
        console.log();
        shellJs.exit(0);
    } catch (error) {
        console.log();
        console.log(`ERROR WITH UNUSED EXPORTS: ${(error as Error).message}`);
        console.log();
        shellJs.exit(1);
    }
}

main();
