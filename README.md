# @ridesz/usual-unused-exports-checker

The typical unused exports checking config I use for my TypeScript projects.

## How to use

### `package.json` setup

Just please add this `scripts` tag part to your `package.json` file too:

```json
{
    "test:exports": "npx @ridesz/usual-unused-exports-checker",
}
```

### GitLab CI config

Please add the necessary parts to your `.gitlab-ci.yml` file:

```yml
image: node:latest

stages:
    - test

exports:
    stage: test
    artifacts:
        paths:
            - build/
        expire_in: 1 week
        when: always
    script:
        - npm install
        - npm rebuild
        - npm run test:exports
```

This would use the latest node image and do the unused exports check as part of the `test` stage.

### Running manually


If you would like to run the unused exports check manually then you can use the following command:

```bash
npm run test:exports
```

## Some implementation details

The checker skips the unused exports in the `src/index.ts` file.
